[TOC]

# Filtrar els registres d'un model segons l'usuari loginat #
A **admin.py**, cal sobreescriure el mètode *get_queryset* del *ModelAdmin*. Ex:

    from .models import MyModel

    class MyAdmin(admin.ModelAdmin):
        """Capçalera."""

        def get_queryset(self, request):
            """Override method."""
            queryset = super().get_queryset(request)

            # Filtrem el que vulguem
            queryset = queryset.filter(...)

            return queryset

    admin.site.register(MyModel, MyAdmin)

Això farà que els registres que haguem exclòs no apareguin al llistat corresponent. A més, si l'usuari edita un registre permès i modifica la URL per intentar editar-ne un que no té permès, tampoc ho permet.

**Aplicacions**

> Ho podem fer servir si volem que cada usuari sigui propietari d'uns registres i només pugui veure i gestionar els seus:

    # Si no és un superusuari, mostrem els registres de l'usuari loginat.
    # Assumim que el model té un camp user
    if not request.user.is_superuser:
        queryset = queryset.filter(user=request.user)

> O per restringir el tipus de registres que pot veure l'usuari segons el rol que tingui assignat:

    # Afegim filtres segons rol
    try:
        # Si la següent línia no falla, és que pertany al grup de convidats.
        request.user.groups.get(name='Convidat')
        # I llavors li deixarem veure només els registres públics.
        # Assumim que el model té un camp "public".
        queryset = queryset.filter(public=True)
    except Exception:
        # Si la línia anterior falla és que no és un convidat i no fem res,
        # ho mostrem tot.
        pass

# Afegir un cercador al llistat #
A **admin.py**:

    from .models import MyModel

    class MyAdmin(admin.ModelAdmin):
        """Capçalera."""

        search_fields = ('nom', 'descripcio', 'customer__category__name')

    admin.site.register(MyModel, MyAdmin)

# Afegir filtres al llistat #
A **admin.py**:

    from .models import MyModel

    class MyAdmin(admin.ModelAdmin):
        """Capçalera."""

        list_filter = ('choice_field', 'foreign_key', 'm2m', 'charfield', 'foreignkey__relatedmodel__attribute')

    admin.site.register(MyModel, MyAdmin)

# Canviar el títol del backend #
El títol apareix a tres llocs: a la capçalera de l'administrador, a la pestanya del navegador (etiqueta \<title>) i a la pàgina de login.

Per canviar-los a tots tres, a l'**admin.py** (o **urls.py**) cal afegir:

    # Etiqueta <title>
    admin.sites.AdminSite.site_title = 'Títol eixerit'
    # Login
    admin.sites.AdminSite.site_header = 'Títol eixerit'
    # Capçalera
    admin.sites.AdminSite.index_title = 'Títol eixerit'

Si el que volem és que el títol canvïi segons l'usuari loginat ho podem fer [canviant la configuració del site dinàmicament](#markdown-header-canviar-la-configuracio-del-site-segons-lusuari-loginat).

# Canviar la configuració del site segons l'usuari loginat #
A **admin.py**:

    class CustomAdminSite(admin.AdminSite):
        """Customitza el site."""

        def each_context(self, request):
            """Customitza el site segons l'usuari."""
            context = super().each_context(request)
            title = 'Hola {}!'.format(request.user.first_name)
            context['site_title'] = title
            context['site_header'] = title
            context['site_url'] = 'https://servidor.com/{}'.format(request.user.username)
            return context

    admin.site = CustomAdminSite()
    admin.autodiscover()

# Personalitzar l'estil del backend #

# Extendre el sistema d'usuaris #
Hi ha diverses opcions per a extendre el sistema d'usuaris de Django. Aquí està molt ben explicat:

https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html

A mode de conclusions, les dues últimes opcions jo no les he provat mai i no acabo de veure el cas d'ús on calguin. Entre les dues primeres la qüestió és si necessitem desar més dades apart de les que té el model bàsic de Django o si només volem afegir alguna funcionalitat a l'usuari.

# Redirigir l'usuari a una pàgina o altra del backend #
A **views.py**:

    from django.contrib.auth import views as views_auth

    class LoginView(views_auth.LoginView):
        """Custom Login View."""

        redirect_authenticated_user = True

        def get_success_url(self):
            """Si no ets el superusuari, que et porti al teu perfil."""
            if self.request.user.is_superuser:
                return reverse('admin:index')
            else:
                return reverse('admin:appname_myprofilemodel_change',
                               args=[self.request.user.pk])

A **urls.py**:

    from .views import LoginView

    urlpatterns = [
        path('admin/login/', LoginView.as_view(), name='login'),
        path('admin/', admin.site.urls),
        ...
    ]

És important que primer definim el nostre login i després l'admin de Django. Si ho fem al revés, no executarà la vista que acabem de crear.


# Filtrar els registres d'un ForeignKey #
A vegades ens pot interessar que en el formulari d'un model que té un ForeignKey, enlloc d'aparèixer tots els registres de la taula on apunta, hi aparegui només una selecció.

Per fer-ho, cal sobreescriure el mètode *render_change_form()* de *ModelAdmin*. Aquest mètode té els següents kwargs:

- Un booleà que indica si estem creant un nou registre

    add=False

- Un booleà que indica si estem modificant un registre existent

    change=False

- Un string que indica la url del formulari

    form_url=''

- L'objecte que estem modificant, en cas que ho fem

    obj=None

A **admin.py**:

    class MyAdmin(admin.ModelAdmin):
        """Custom admin."""

        def render_change_form(self, request, context, add=False, change=False, form_url='',
                               obj=None):
            """Override choices."""
            # Recollim els camp del formulari que volem sobreescriure
            camp = context['adminform'].form.fields['forana']
            # El sobreescrivim
            context['adminform'].form.fields['forana'].queryset = camp.queryset.filter(...)
            # Fem la crida al super()
            return super().render_change_form(request, context, add, change, form_url, obj)

Aquest mateix procés el podem fer servir per sobreescriure qualsevol altre camp.

**Aplicacions**

> Ho podem fer servir per tal que els usuaris amb un determinat rol només puguin triar entre les opcions que el superusuari ha activat en aquell moment.

    context['adminform'].form.fields['forana'].queryset = camp.queryset.filter(active=True)

> O, per exemple, si tenim un model de clients que té treballados i defineix tasques assignades a aquests treballados. A la fitxa de tasca, el client només ha de veure els seus treballadors.

    class TaskAdmin(admin.ModelAdmin):
        """Custom admin."""

        def render_change_form(self, request, context, add=False, change=False, form_url='',
                               obj=None):
            """Override choices."""
            # Agafem el camp treballador de la tasca
            treballadors = context['adminform'].form.fields['treballador']
            # N'agafem el queryset filtrant-lo segons l'usuari
            queryset = treballadors.queryset.filter(user=request.user)
            # Actualitzem el queryset a context
            context['adminform'].form.fields['treballador'].queryset = queryset
            # Fem la crida al super()
            return super().render_change_form(request, context, add, change, form_url, obj)

# Afegir la funcionalitat "password reset" al login #

A **urls.py**, afegir aquests 4 primers paths (abans del path admin/):

    urlpatterns = [
        path('admin/password_reset/',
             auth_views.PasswordResetView.as_view(),
             name='admin_password_reset'),
        path('admin/password_reset/done/',
             auth_views.PasswordResetDoneView.as_view(),
             name='password_reset_done',),
        path('reset/<uidb64>/<token>/',
             auth_views.PasswordResetConfirmView.as_view(),
             name='password_reset_confirm',),
        path('reset/done/',
             auth_views.PasswordResetCompleteView.as_view(),
             name='password_reset_complete',),
        path('admin/', admin.site.urls),
    ]

# Fer desplegables amb jerarquía d'opcions #

El Django permet fer desplegables amb títols (o desplegables amb jerarquía d'opcions), com això:

https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_optgroup

## ChoiceField ##

Per fer-ho només cal que les opcions (choices) del camp en qüestió tinguin una estructura amb més nivells.

A **models.py** enlloc de:

    CHOICES = (
      ('clau1', 'Opció 1'),
      ('clau2', 'Opció 2'),
      ('clau3', 'Opció 3'),
      ('clau4', 'Opció 4')
    )
    camp = models.ChoiceField(choices=CHOICES)

Podem fer:

    CHOICES = (
        ('Títol 1', (
            ('clau1', 'Opció 1'),
            ('clau2', 'Opció 2'),
        )),
        ('Títol 2', (
            ('clau3', 'Opció 3'),
            ('clau4', 'Opció 4'),
        )),
    )
    camp = models.ChoiceField(choices=CHOICES)

## ForeignKeyField ##

Això mateix també es pot fer quan el camp és de tipus **ForeignKey** o **ManyToMany**, però com que les opcions les hem de treure dinàmicament d'una o més taules això ja no ho podem fer a **models.py** i cal fer-ho a **admin.py**, sobreescrivint el mètode **render_change_form** de **ModelAdmin** i construïnt les choices dinàmicament:

    def render_change_form(self, request, context, *args, **kwargs):
        """Override choices."""
        groupped = []
        for ambit in Ambit.objects.all().order_by('ordre'):
            group = [
                ambit.nom, []
            ]
            for subambit in ambit.subambits.all().order_by('nom'):
                group[1].append([subambit.pk, subambit.nom])
            groupped.append(group)
        # sobreescrivim les choices del camp que volem canviar: subambit
        context['adminform'].form.fields['subambit'].choices = groupped
        return super().render_change_form(request, context, *args, **kwargs)

> Aquest mètode, però, només afecta a la vista del formulari. Si tenim un llistat que permet editar i algún dels camps és desplegable, no entra a aquest mètode i, per tant, no farà l'agrupació.

## ManyToManyField ##

En el cas dels ManyToManyField, tècnicament no és diferent del ForeignKeyField. Hem de fer el mateix i de la mateixa manera.

La diferència és si volem un ManyToManyField que es mostri amb el widget filter_horizontal. Aquest widget es construeix amb Javascript i el Javasript que porta el Django no està pensat per funcioanr amb optgroups.

Per a que funcioni amb optgroups cal sobreescriure aquest JavaScript amb la versió que podem trobar a:

https://code.djangoproject.com/raw-attachment/ticket/13883/SelectBox.js

> Aquest JavaScript té un error a la línia 50. Enlloc de toLowerCase.indexOf cal que digui toLowerCase().indexOf.

Només cal posar aquest JS a la carpeta static de la nostra app i el Django sobreescriurà l'original.


# Posar pestanyes a un formulari de model #

A vegades tenim un model que té molts camps i ens pot interessar que el formulari d'edició estigui una mica més endreçat. Organitzar els diferents camps en diferents pestanyes és una bona manera de no saturar la pantalla amb informació.

Sembla que és un paquet abandonat però jo el faig servir habitualment i fa el fet prou bé: django-tabbed-admin

> En podeu trobar tota la informació a: https://github.com/omji/django-tabbed-admin

Un cop activat, podeu eliminar la barra de color blau de les pestanyes afegint un CSS a aquesta vista. Per fer-ho, cal afegir el següent codi al ModelAdmin on tenim les pestanyes:

    class Media:
        """Media."""

        css = {
            'all': ('admin/css/custom-tabbed-admin.css',)
            }

Haureu de posar-hi un CSS que sobreescrigui el CSS original del jQuery. Com per exemple, el que [podeu trobar aquí](resources/tabbed-admin/custom-tabbed-admin.css).
