[TOC]

# Get streets in a city (by name) #

    [out:json];
    area[name = "Girona"][boundary="administrative"][admin_level="8"];
    (way(area)["highway"~"^(motorway|trunk|primary|secondary|tertiary|residential|unclassified|living_street|service|pedestrian|track|road)$"];>;);
    out;

# Get streets in a polygon #

    [out:json];
    (way(poly:"50.7 7.1 50.7 7.12 50.71 7.11")["highway"~"^(motorway|trunk|primary|secondary|tertiary|residential|unclassified|living_street|service|pedestrian|track|road)$"];>;);
    out;
